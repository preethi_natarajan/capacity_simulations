#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/lte-module.h"
#include "ns3/config-store.h"
#include "ns3/propagation-environment.h"
#include "ns3/channel-condition-model.h"
#include <iomanip>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

using namespace ns3;

int
main (int argc, char *argv[]){

	uint32_t nEnbPerRow = 1;
	uint32_t nUePerRow = 1;
	uint32_t simTime = 500;
	uint8_t freqReUseFactor = 1;
	bool ueAttachToClosestEnb = true;
	int8_t shiftBy = 0.0;
	CommandLine cmd (__FILE__);

	cmd.AddValue ("nEnbPerRow", "Number of eNodeBs per Row", nEnbPerRow);
	cmd.AddValue ("nUePerRow", "Number of UEs per Row", nUePerRow);
	cmd.AddValue ("freqReUse", "Frequency reuse factor", freqReUseFactor);
	cmd.AddValue ("ueAttachToClosestEnb", "Frequency reuse factor", ueAttachToClosestEnb);
	cmd.AddValue ("shiftBy", "Shift UE placement by meters", shiftBy);
	cmd.AddValue ("simTime", "Total duration of the simulation (in milliseconds)",
				simTime);
	cmd.Parse (argc, argv);

	ConfigStore inputConfig;
	inputConfig.ConfigureDefaults ();

	// parse again so you can override default values from the command line
	cmd.Parse (argc, argv);

	// Geometry of the scenario (in meters)
	//  double eNBHeight = 12; // 3 story building height
	double eNBHeight = 25; // per Shashi
	//  double eNBHeight = 32; // per double eNBHeight = 25; // per ShashiShashi

	double ueHeight = 1.5;
	//  double spaceBwEnb = 95; // around 300 ft between APs.
	double spaceBwEnb = 200; // per Sushanth, for cost231

	bool enableUplinkPwrCtrl = false;
	Config::SetDefault ("ns3::LteUePhy::EnableUplinkPowerControl", BooleanValue (enableUplinkPwrCtrl));

	// total number of eNBs
	uint32_t nEnb = nEnbPerRow * nEnbPerRow;

	Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
	lteHelper->SetSchedulerType ("ns3::PfFfMacScheduler");
	NS_LOG_UNCOND("Scheduler: " << lteHelper->GetSchedulerType());

	// PN
	// Increase SRS periodicity to attach more than 40 UEs
	uint16_t srsPeriodicity = 320;
	Config::SetDefault("ns3::LteEnbRrc::SrsPeriodicity", UintegerValue (srsPeriodicity));

	// PN
	// Apart from distance, 3gpp UMA has a variability component based on shadowing
	lteHelper->SetAttribute ("PathlossModel",
	//                               StringValue ("ns3::OkumuraHataPropagationLossModel"));
	//                               StringValue ("ns3::ThreeGppUmaPropagationLossModel"));
							   StringValue("ns3::Cost231PropagationLossModel"));
	//  EnvironmentType envType = SubUrbanEnvironment;
	//  lteHelper->SetPathlossModelAttribute ("Environment", EnumValue (envType));
	lteHelper->SetPathlossModelAttribute ("Frequency", DoubleValue (3.6e9));
	lteHelper->SetPathlossModelAttribute ("BSAntennaHeight", DoubleValue (eNBHeight));
	lteHelper->SetPathlossModelAttribute ("SSAntennaHeight", DoubleValue (ueHeight));
	//  lteHelper->SetPathlossModelAttribute ("ShadowingEnabled", BooleanValue (false));
	//  Ptr<ChannelConditionModel> condModel = CreateObject <ThreeGppUmaChannelConditionModel> ();
	//  lteHelper->SetPathlossModelAttribute("ChannelConditionModel", PointerValue(condModel));

	// Set Tx power of eNB
	// Per Shashi -- Baicells uses this
	//  double eNbTxPower = 33;
	double eNbTxPower = 44;
	Config::SetDefault ("ns3::LteEnbPhy::TxPower", DoubleValue(eNbTxPower));

	// write details to file
	std::ofstream outfile("ns3-grid-positions-log.txt");
	std::ofstream posfile("ns3-grid-positions.txt");
	posfile << "type\tid\tx\ty\n";

	lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (100));
	lteHelper->SetEnbDeviceAttribute ("UlBandwidth", UintegerValue (100));

	MobilityHelper mobility;
	mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
	// std::vector<Vector> enbPosition;
	Ptr<ListPositionAllocator> eNBPositionAlloc = CreateObject<ListPositionAllocator> ();

	// Position and install eNB devices
	uint32_t plantedEnb = 0;

	Ptr<Node> enbNode = NULL;
	MobilityHelper ueMobility;
	ueMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");

	// Create Nodes: eNodeB and UE
	NodeContainer enbNodes;
	enbNodes.Create(nEnb);

	uint32_t gridLength = spaceBwEnb * nEnbPerRow;

	for (uint32_t row = 0; row < nEnbPerRow; row++)
	{
	  for (uint32_t column = 0; column < nEnbPerRow && plantedEnb < nEnb; column++, plantedEnb++)
		{

		  outfile << "Row: " << row << ", Column: " << column << ", PlantedEnb: " << plantedEnb + 1 << "\n";
		  Vector v (spaceBwEnb * (row + 0.5), spaceBwEnb * (column + 0.5), eNBHeight);
		  outfile << "EnB " << plantedEnb + 1 << " Position: " << v << "\n";
		  posfile << "enb\t" << plantedEnb + 1 << "\t" << v.x << "\t" << v.y << "\n";

		  eNBPositionAlloc->Add (v);
		}
	}
	mobility.SetPositionAllocator (eNBPositionAlloc);
	mobility.Install (enbNodes);

	// total number of UEs
	uint32_t nUe = nUePerRow * nUePerRow;

	NodeContainer ueNodes;
	ueNodes.Create(nUe);
	// Position of UEs
	uint32_t plantedUe = 0;
	double spaceBwUe = std::ceil (gridLength * 0.9 / nUePerRow);
	outfile << "spaceBwUe: " << spaceBwUe << "\n";
	Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
	for (uint32_t row = 0; row < nUePerRow; row++)
	{
	  for (uint32_t column = 0; column < nUePerRow && plantedUe < nUe; column++, plantedUe++)
		{
		  outfile << "Row: " << row << ", Column: " << column << ", PlantedUe: " << plantedUe + 1 << "\n";
		  Vector v (spaceBwUe * (row + 0.5) + shiftBy, spaceBwUe * (column + 0.5) + shiftBy, ueHeight);
		  outfile << "UE " << plantedUe + 1 << " Position: " << v << "\n";
		  positionAlloc->Add (v);
		  posfile << "ue\t" << plantedUe + 1 << "\t" << v.x << "\t" << v.y << "\n";
		}
	}

	mobility.SetPositionAllocator (positionAlloc);
	mobility.Install (ueNodes);

	NetDeviceContainer enbDevs;

	int freqTable[2][2] = {
		{38650, 38650},
		{39649, 39649}
	};
	// Set center frequency for each eNB
	LteEnbNetDevice enbDev;
	if (freqReUseFactor > 1) {
	 plantedEnb = 0;
	 uint32_t freqType = 0;
	 for (uint32_t row = 0; row < nEnbPerRow; row++)
	 {
	  for (uint32_t column = 0; column < nEnbPerRow && plantedEnb < nEnb; column++, plantedEnb++)
	  {
		freqType = (row + column) % freqReUseFactor;
		lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (freqTable[freqType][0]));
		lteHelper->SetEnbDeviceAttribute ("UlEarfcn", UintegerValue (freqTable[freqType][1]));

		outfile << "EnB: " << plantedEnb + 1 << ", freq_type: " << freqType << "\n";
		enbDevs.Add(lteHelper->InstallEnbDevice(enbNodes.Get(plantedEnb)));
	  }
	 }
	}
	else {
	 enbDevs = lteHelper->InstallEnbDevice (enbNodes);
	}

	// Create UE Devices and install them
	NetDeviceContainer ueDevs;
	ueDevs = lteHelper->InstallUeDevice (ueNodes);

	// Attach UEs to closest eNB
	lteHelper->AttachToClosestEnb (ueDevs, enbDevs);

	// Activate a data radio bearer
	enum EpsBearer::Qci q = EpsBearer::GBR_CONV_VOICE;
	EpsBearer bearer (q);
	lteHelper->ActivateDataRadioBearer (ueDevs, bearer);

	outfile.close();
	posfile.close();

	Simulator::Stop (MilliSeconds (simTime));
	lteHelper->EnablePhyTraces ();

	Simulator::Run ();

	/*GtkConfigStore config;
	config.ConfigureAttributes ();*/

	Simulator::Destroy ();


	return 0;
}
