import pandas as pd
import sys

def process_trace(trace_location='datasets/grid/fr1'
                  ):

    print ("Processing {}..".format(trace_location + '/DlRsrpSinrStats.txt'))

    dl_df = pd.read_csv(trace_location + '/DlRsrpSinrStats.txt', sep='\t')
    dl_df = dl_df.rename(columns={"IMSI": "imsi", "cellId": "cell_id"})

    dedup_df = dl_df[['cell_id', 'imsi']].drop_duplicates(subset='imsi', keep="last")

    sect_df = pd.read_csv(trace_location + '/ns3-grid-sectors.txt', sep='\t')
    dedup_df = pd.merge(dedup_df, sect_df, how='left', left_on='cell_id', right_on='sector_id')

    for freq in sect_df.freq2_freq.unique():
        dedup_df.loc[(dedup_df.freq2_freq == freq)]['imsi'].to_csv(trace_location + '/freqType_{}.csv'.format(freq),
                                                             index=False,
                                                            header=False)

    return dedup_df

if __name__ == "__main__":
    print ("Arguments: {}".format(str(sys.argv)))
    process_trace(
        trace_location = sys.argv[1])