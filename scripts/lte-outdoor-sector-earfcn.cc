#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/lte-module.h"
#include "ns3/config-store.h"
#include "ns3/propagation-environment.h"
#include "ns3/channel-condition-model.h"
#include "ns3/point-to-point-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include <iomanip>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

using namespace ns3;

int
main (int argc, char *argv[]){

	uint32_t nEnbPerRow = 1;
	uint32_t nUePerRow = 1;
	uint32_t simTime = 500;
	uint8_t freqReUseFactor = 2;
	bool freqReUse = false;
	uint8_t rngRun = 3;
	uint8_t nSectors = 4;
	std::string uesToDropFile = "uesToDrop.txt";
	uint32_t freqType = 0;

	// (9000 bytes per 1ms) == 72Mbps
	uint32_t udpPacketSize = 9000;
	 Time interPacketInterval = MilliSeconds(1);
	//  uint32_t udpPacketSize = 9000;
	//  uint32_t udpPacketSize = 100;


	bool disableDl = false;
	bool disableUl = false;
	CommandLine cmd (__FILE__);

	cmd.AddValue ("nEnbPerRow", "Number of eNodeBs per Row", nEnbPerRow);
	cmd.AddValue ("nUePerRow", "Number of UEs per Row", nUePerRow);
	cmd.AddValue ("freqReUse", "Frequency reuse factor", freqReUse);
	cmd.AddValue ("freqType", "Center frequency to use (valid only for freq Reuse = True)", freqType);
	cmd.AddValue ("rngRun", "Random stream run number", rngRun);
	cmd.AddValue ("interPacketInterval", "Inter packet interval", interPacketInterval);
	cmd.AddValue ("uesToDropFile", "Location of file containing UEs to drop from simulation", uesToDropFile);
	cmd.AddValue ("disableDl", "Disable downlink data flows", disableDl);
	cmd.AddValue ("disableUl", "Disable uplink data flows", disableUl);
	cmd.AddValue ("simTime", "Total duration of the simulation (in milliseconds)",
				simTime);
	cmd.Parse (argc, argv);

	ConfigStore inputConfig;
	inputConfig.ConfigureDefaults ();

	// parse again so you can override default values from the command line
	cmd.Parse (argc, argv);

	// Geometry of the scenario (in meters)
	//  double eNBHeight = 12; // 3 story building height
	double eNBHeight = 25; // per Shashi
	//  double eNBHeight = 32; // per double eNBHeight = 25; // per ShashiShashi

	double ueHeight = 1.5;
	//  double spaceBwEnb = 95; // around 300 ft between APs.
	//  double spaceBwEnb = 200; // per Sushanth, for cost231
	double spaceBwEnb = 400;

	// total number of eNBs
	uint32_t nEnb = nEnbPerRow * nEnbPerRow;

	Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
	Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
	lteHelper->SetEpcHelper (epcHelper);

	Ptr<Node> pgw = epcHelper->GetPgwNode ();

	// Create a single RemoteHost
	NodeContainer remoteHostContainer;
	remoteHostContainer.Create (1);
	Ptr<Node> remoteHost = remoteHostContainer.Get (0);
	InternetStackHelper internet;
	internet.Install (remoteHostContainer);

	// Create the Internet
	PointToPointHelper p2ph;
	p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
	p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
	p2ph.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (10)));
	NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);
	Ipv4AddressHelper ipv4h;
	ipv4h.SetBase ("1.0.0.0", "255.0.0.0");
	Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (internetDevices);
	// interface 0 is localhost, 1 is the p2p device
	Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress (1);

	Ipv4StaticRoutingHelper ipv4RoutingHelper;
	Ptr<Ipv4StaticRouting> remoteHostStaticRouting = ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
	remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);

	// Set the scheduler
	lteHelper->SetSchedulerType ("ns3::PfFfMacScheduler");
	//  lteHelper->SetSchedulerType ("ns3::RrFfMacScheduler");
	NS_LOG_UNCOND("Scheduler: " << lteHelper->GetSchedulerType());

	// PN
	// Increase SRS periodicity to attach more than 40 UEs
	uint16_t srsPeriodicity = 320;
	Config::SetDefault("ns3::LteEnbRrc::SrsPeriodicity", UintegerValue (srsPeriodicity));

	lteHelper->SetAttribute ("PathlossModel",
							   StringValue("ns3::Cost231PropagationLossModel"));
	lteHelper->SetPathlossModelAttribute ("Frequency", DoubleValue (3.6e9));
	lteHelper->SetPathlossModelAttribute ("BSAntennaHeight", DoubleValue (eNBHeight));
	lteHelper->SetPathlossModelAttribute ("SSAntennaHeight", DoubleValue (ueHeight));

	// Set Tx power of eNB
	// Per Shashi -- Baicells uses this
	//  double eNbTxPower = 33;
	double eNbTxPower = 33; // + 17 dbm Antenna gain
	//  double eNbTxPower = 47;
	Config::SetDefault ("ns3::LteEnbPhy::TxPower", DoubleValue(eNbTxPower));

	// write details to file
	std::ofstream outfile("ns3-grid-positions-log.txt");
	std::ofstream posfile("ns3-grid-positions.txt");
	posfile << "type\tid\tsector_id\tx\ty\n";
	std::ofstream sectfile("ns3-grid-sectors.txt");
	sectfile << "sector_id\tfreq2_freq\tcurr_freq\n";

	MobilityHelper mobility;
	mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
	Ptr<ListPositionAllocator> eNBPositionAlloc = CreateObject<ListPositionAllocator> ();

	/// Position and install eNB devices
	uint32_t plantedEnb = 1;
	uint32_t plantedSector = 1;

	// Create Nodes: eNodeB and UE
	NodeContainer enbNodes;
	enbNodes.Create(nEnb * nSectors);

	uint32_t gridLength = spaceBwEnb * nEnbPerRow;

	// offset for each sector in meters
	double sectorOffset[4][2] = {
		{0.25, 0},
		{0, 0.25},
		{-0.25, 0},
		{0, -0.25}
	};

	for (uint32_t row = 0; row < nEnbPerRow; row++)
	{
	  for (uint32_t column = 0; column < nEnbPerRow && plantedEnb <= nEnb; column++, plantedEnb++)
		{

		for (uint32_t sector = 0; sector < nSectors; sector++, plantedSector++)
			{

				  Vector v (spaceBwEnb * (row + 0.5) + sectorOffset[sector][0],
							spaceBwEnb * (column + 0.5) + sectorOffset[sector][1],
							eNBHeight);
				  outfile << "Row: " << row << ", Column: " << column << ", Enb: " << plantedEnb << ", Sector: " << plantedSector << ", Position: " << v << "\n";
				  posfile << "enb\t" << plantedEnb  << "\t" << plantedSector << "\t" << v.x << "\t" << v.y << "\n";
				  eNBPositionAlloc->Add (v);
			}
		}
	}
	mobility.SetPositionAllocator (eNBPositionAlloc);
	mobility.Install (enbNodes);

	// total number of UEs
	uint32_t nUe = nUePerRow * nUePerRow;
	// which Ues to drop
	std::ifstream uesToDropInFile(uesToDropFile);
	std::vector<uint32_t> uesToDrop;
	int number = -1;
	while(uesToDropInFile >> number)
	{
	  uesToDrop.push_back(number);
	}
	uesToDropInFile.close();
	std::sort(uesToDrop.begin(), uesToDrop.end());
	NS_LOG_UNCOND(uesToDrop.size() << " uesToDrop" );

	NodeContainer ueNodes;
	NodeContainer attachUeNodes;
	ueNodes.Create(nUe);

	double spaceBwUe = std::ceil (gridLength / nUePerRow);
	RngSeedManager::SetSeed (3);  // Changes seed from default of 1 to 3
	RngSeedManager::SetRun (rngRun);
	Ptr<UniformRandomVariable> shiftRng = CreateObject<UniformRandomVariable> ();
	shiftRng->SetAttribute ("Min", DoubleValue (spaceBwUe * -0.1));
	shiftRng->SetAttribute ("Max", DoubleValue (spaceBwUe * 0.1));
	double shiftBy = shiftRng->GetValue();
	//  double shiftXBy = shiftRng->GetValue();
	uint32_t plantedUe = 1;
	uint32_t indNextUeToDrop = 0;
	outfile << "spaceBwUe: " << spaceBwUe << ", shiftBy: " << shiftBy << "\n";

	// Position of UEs
	Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
	for (uint32_t row = 0; row < nUePerRow; row++)
	{
	  for (uint32_t column = 0; column < nUePerRow && plantedUe <= nUe; column++, plantedUe++)
		{
		  outfile << "Row: " << row << ", Column: " << column << ", PlantedUe: " << plantedUe << "\n";
		  Vector v (spaceBwUe * (row + 0.5) + shiftBy, spaceBwUe * (column + 0.5) + 2 * shiftBy, ueHeight);
		  outfile << "UE " << plantedUe << " Position: " << v << "\n";
		  positionAlloc->Add (v);
		  posfile << "ue\t" << plantedUe << "\t" << "-1" << "\t" << v.x << "\t" << v.y << "\n";

		  // Should we drop this UE?
		  if ((uesToDrop.size() > 0) && (uesToDrop[indNextUeToDrop] == plantedUe)) {
			outfile << "Dropping IMSI: " << plantedUe << "\n";
			indNextUeToDrop++;
		  }
		  else {
			attachUeNodes.Add(ueNodes.Get(plantedUe-1));
			outfile << "Attaching IMSI: " << plantedUe << "\n";
		  }
		}
	}

	NS_LOG_UNCOND(attachUeNodes.GetN() << " attachUeNodes ");
	mobility.SetPositionAllocator (positionAlloc);
	mobility.Install (ueNodes);

	//  double sectorOrientation[4] = {0.0, 90.0, -180.0, -90.0};
	double sectorOrientation[4] = {0.0, 90.0, 180.0, 270.0};
	double orientation = 0.0;

	NetDeviceContainer enbDevs;

	lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (100));
	lteHelper->SetEnbDeviceAttribute ("UlBandwidth", UintegerValue (100));

	int freqTable[2][2] = {
			{100, 18100},
			{100, 18100}
	};

	NS_LOG_UNCOND("freqType: " << freqType);
	plantedSector = 0;
	uint32_t freq2_freq = 0;
	for (uint32_t row = 0; row < nEnbPerRow; row++)
	 {
	  for (uint32_t column = 0; column < nEnbPerRow && plantedSector < nEnb * nSectors; column++)
	  {
		for (uint32_t sector = 0; sector < nSectors; sector++, plantedSector++)
		{
			orientation = sectorOrientation[sector];
			lteHelper->SetEnbAntennaModelType ("ns3::CosineAntennaModel");
			lteHelper->SetEnbAntennaModelAttribute ("Orientation", DoubleValue (orientation));
			lteHelper->SetEnbAntennaModelAttribute ("Beamwidth",   DoubleValue (55.0));
			lteHelper->SetEnbAntennaModelAttribute ("MaxGain",     DoubleValue (17.0));
			freq2_freq = (row + column + sector) % freqReUseFactor;
			if (freqReUse) {

				lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (freqTable[freqType][0]));
				lteHelper->SetEnbDeviceAttribute ("UlEarfcn", UintegerValue (freqTable[freqType][1]));
			}
			outfile << "Sector: " << plantedSector + 1 << ", freq2_freq: " << freq2_freq <<
				", curr_freq: " << freqType <<  ", Orientation: " << orientation << "\n";
			sectfile << plantedSector + 1 << "\t" << freq2_freq << "\t" << freqType << "\n";
			enbDevs.Add(lteHelper->InstallEnbDevice(enbNodes.Get(plantedSector)));
		}
	  }
	}

	// Create UE Devices and install them
	NetDeviceContainer ueDevs;
	NetDeviceContainer attachUeDevs;
	ueDevs = lteHelper->InstallUeDevice (ueNodes);
	indNextUeToDrop = 0;
	for (uint32_t u = 0; u < ueNodes.GetN (); ++u)
	 {
		// Should we drop this UE?
		  if ((uesToDrop.size() > 0) && (uesToDrop[indNextUeToDrop] == u + 1)) {
			indNextUeToDrop++;
		  }
		  else {
			attachUeDevs.Add(ueDevs.Get(u));
		  }
	 }


	// Install the IP stack on the UEs
	internet.Install (attachUeNodes);
	Ipv4InterfaceContainer ueIpIface;
	ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (attachUeDevs));
	// Assign IP address to UEs, and install applications
	indNextUeToDrop = 0;
	for (uint32_t u = 0; u < attachUeNodes.GetN (); ++u)
	 {
	   Ptr<Node> ueNode = attachUeNodes.Get (u);
	   // Set the default gateway for the UE
	   Ptr<Ipv4StaticRouting> ueStaticRouting = ipv4RoutingHelper.GetStaticRouting (ueNode->GetObject<Ipv4> ());
	   ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
	 }

	// Attach UEs based on cell selection
	// will activate default bearer
	lteHelper->Attach (attachUeDevs);

	// Install and start applications on UEs and remote host
	uint16_t dlPort = 1100;
	uint16_t ulPort = 2000;
	ApplicationContainer clientApps;
	ApplicationContainer serverApps;
	for (uint32_t u = 0; u < attachUeNodes.GetN (); ++u)
	 {
	   if (!disableDl)
		 {
		   PacketSinkHelper dlPacketSinkHelper ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), dlPort));
		   serverApps.Add (dlPacketSinkHelper.Install (attachUeNodes.Get(u)));

		   UdpClientHelper dlClient (ueIpIface.GetAddress (u), dlPort);
		   dlClient.SetAttribute ("Interval", TimeValue (interPacketInterval));
		   dlClient.SetAttribute ("PacketSize", UintegerValue (udpPacketSize));
		   dlClient.SetAttribute ("MaxPackets", UintegerValue (1000000));
		   clientApps.Add (dlClient.Install (remoteHost));
		 }

	   if (!disableUl)
		 {
		   ++ulPort;
		   PacketSinkHelper ulPacketSinkHelper ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), ulPort));
		   serverApps.Add (ulPacketSinkHelper.Install (remoteHost));

		   UdpClientHelper ulClient (remoteHostAddr, ulPort);
		   ulClient.SetAttribute ("Interval", TimeValue (interPacketInterval));
		   ulClient.SetAttribute ("PacketSize", UintegerValue (udpPacketSize));
		   ulClient.SetAttribute ("MaxPackets", UintegerValue (1000000));
		   clientApps.Add (ulClient.Install (attachUeNodes.Get(u)));
		 }

	 }
	serverApps.Start (MilliSeconds (250));
	clientApps.Start (MilliSeconds (251));

	outfile.close();
	posfile.close();
	sectfile.close();

	Simulator::Stop (MilliSeconds (simTime));
	lteHelper->EnableTraces ();

	Simulator::Run ();

	/*GtkConfigStore config;
	config.ConfigureAttributes ();*/

	Simulator::Destroy ();


	return 0;
}
