#!/bin/bash

set -x
echo "Beginning simulations..."
echo `date`

nEnbPerRow=5
nUePerRow=34
script_loc="scratch/lte-outdoor-sector-earfcn"
simTime=750
rngRun=3
simDir="/home/ubuntu/ns3/capacity_simulations/datasets/run_$rngRun"

sim_args="$script_loc --rngRun=$rngRun --nEnbPerRow=$nEnbPerRow --nUePerRow=$nUePerRow --simTime=$simTime"
echo $sim_args

rm -rf $simDir
mkdir -p $simDir/fr1
time ./waf --cwd $simDir/fr1 --run "$sim_args"

echo "Processing UEs by frequency... "
python3 ./ues_by_frequency.py $simDir/fr1

#simTime=500
sim_args="$script_loc --uesToDropFile=$simDir/fr1/freqType_1.csv --rngRun=$rngRun --nEnbPerRow=$nEnbPerRow --nUePerRow=$nUePerRow --freqReUse=true --simTime=$simTime"
mkdir -p $simDir/fr2/freqType0
time ./waf --cwd $simDir/fr2/freqType0 --run "$sim_args"

sim_args="$script_loc --uesToDropFile=$simDir/fr1/freqType_0.csv --rngRun=$rngRun --nEnbPerRow=$nEnbPerRow --nUePerRow=$nUePerRow --freqReUse=true --freqType=1 --simTime=$simTime"
mkdir -p $simDir/fr2/freqType1
time ./waf --cwd $simDir/fr2/freqType1 --run "$sim_args"